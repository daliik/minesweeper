package minesweeper;

public class MinesweeperInterface {
	private int frameSize;
	
	
	public MinesweeperInterface(){
		this.frameSize = 0;
		
	}
	public int getFrameSize(){
		return this.frameSize;
	}
	/* sets new value for frameSize and amount of mines so that it could be called
	 * in the Board class
	 */
	public void setFrameSize(int frameSize){
		this.frameSize = frameSize;
	}
	
	}
	

