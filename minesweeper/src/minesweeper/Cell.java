package minesweeper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Cell implements MouseListener {
	private JButton button;
	private Board board;
	private int value;
	private int id;
	private boolean notChecked;
	

	public Cell(Board board) {
		button = new JButton();
		button.addMouseListener(this);
		button.setPreferredSize(new Dimension(20, 20));
		button.setMargin(new Insets(0, 0, 0, 0));
		this.board = board;
		notChecked = true;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public JButton getButton() {
		return button;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public int getValue() {
		return value;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public int getId() {
		return id;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void setId(int id) {
		this.id = id;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void setValue(int value) {
		this.value = value;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void displayValue() {
		if (value == -1) {
			button.setText("\u2600");
			button.setBackground(Color.RED);
		} else if (value != 0) {
			button.setText(String.valueOf(value));
			//System.out.println("this is the bomb count written to the cells:"+countBomb);
		}
	}

	/*
	 * If block is pressed acts accordingly to its value
	 * if its value is o it will scan for empty blocks
	 * if it is -1, it means that you have found the bomb
	 * /* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game 
	 * 
	 */
	public void checkCell() {
		button.setEnabled(false);
		displayValue();
		notChecked = false;
		if (value == 0)
			board.scanForEmptyCells();
		if (value == -1)
			board.fail();

	}
   /*Sets the background color of the block blue*/
	public void flagCell() {
		button.setBackground(Color.BLUE);
	}
	/*Sets block back to its original color*/
	public void unflagCell() {
		button.setBackground(null);

	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void incrementValue() {
		value++;
		
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public boolean isNotChecked() {
		return notChecked;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public boolean isEmpty() {
		return isNotChecked() && value == 0;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void reveal() {
		displayValue();
		button.setEnabled(false);
	}

	/*
	 * These methods checks which mouse button is pressed. If left is pressed then
	 * opens the block, if right mouse button is pressed marks the block blue
	 * and if right is pressed again marks it default color
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			checkCell();
		}
	}

	private boolean isFirstClick = true;

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3 && isFirstClick == true && notChecked == true) {

			isFirstClick = false;
			flagCell();
		} else if (e.getButton() == MouseEvent.BUTTON3 && isFirstClick == false && notChecked == true) {
			isFirstClick = true;
			unflagCell();

		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

}
