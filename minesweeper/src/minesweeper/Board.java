package minesweeper;

import javax.swing.*;

import minesweeper.Frame;

import java.awt.*;

import java.util.ArrayList;


public class Board {
	
	private Cell[][] cells;
	private int cellID = 0;
	public int side = Frame.msi.getFrameSize();
	private int limit = side - 2;



	/* Creates the gaming board
	 * /* from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public void getBoard() {
		JFrame frame = new JFrame();
		frame.add(addCells());

		plantMines();
		setCellValues();

		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	/* This method sets the amount of blocks in the field  and gets the value from frame class button*/
	public JPanel addCells() {
		side = Frame.msi.getFrameSize();
		System.out.println(side);
		JPanel panel = new JPanel(new GridLayout(side, side));
		cells = new Cell[side][side];
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				cells[i][j] = new Cell(this);
				cells[i][j].setId(getID());
				panel.add(cells[i][j].getButton());
			}
		}
		return panel;
	}

	/* Enters the number of mines depending on the field size selected from Frame class */
	public void plantMines() {

		ArrayList<Integer> loc = generateMinesLocation(Frame.msi.getFrameSize());
		for (int i : loc) {
			getCell(i).setValue(-1);
		}
	}

	/* Chooses random places for mines 
	 * /*  tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public ArrayList<Integer> generateMinesLocation(int q) {
		ArrayList<Integer> loc = new ArrayList<Integer>();
		int random;
		for (int i = 0; i < q;) {
			random = (int) (Math.random() * (side * side));
			if (!loc.contains(random)) {
				loc.add(random);
				i++;
			}
		}
		return loc;
	}

	
	/*
	 * This method counts the number of mines around particular cell and sets
	 * its value accordingly
	 * /*  tutoarial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	 
	public void setCellValues() {

		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (cells[i][j].getValue() != -1) {
					if (j >= 1 && cells[i][j - 1].getValue() == -1)
						cells[i][j].incrementValue();
					if (j <= limit && cells[i][j + 1].getValue() == -1)
						cells[i][j].incrementValue();
					if (i >= 1 && cells[i - 1][j].getValue() == -1)
						cells[i][j].incrementValue();
					if (i <= limit && cells[i + 1][j].getValue() == -1)
						cells[i][j].incrementValue();
					if (i >= 1 && j >= 1 && cells[i - 1][j - 1].getValue() == -1)
						cells[i][j].incrementValue();
					if (i <= limit && j <= limit && cells[i + 1][j + 1].getValue() == -1)
						cells[i][j].incrementValue();
					if (i >= 1 && j <= limit && cells[i - 1][j + 1].getValue() == -1)
						cells[i][j].incrementValue();
					if (i <= limit && j >= 1 && cells[i + 1][j - 1].getValue() == -1)
						cells[i][j].incrementValue();
					
				}
			}
		}
	}

	/*
	 * This method starts chain reaction. When user click on particular cell, if
	 * cell is empty (value = 0) this method look for other empty cells next to
	 * activated one. If finds one, it calls checkCell and in effect, start next
	 * scan on its closest area.
	 *  tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game
	 */
	
	public void scanForEmptyCells() {
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (!cells[i][j].isNotChecked()) {
					if (j >= 1 && cells[i][j - 1].isEmpty())
						cells[i][j - 1].checkCell();
					if (j <= limit && cells[i][j + 1].isEmpty())
						cells[i][j + 1].checkCell();
					if (i >= 1 && cells[i - 1][j].isEmpty())
						cells[i - 1][j].checkCell();
					if (i <= limit && cells[i + 1][j].isEmpty())
						cells[i + 1][j].checkCell();
					if (i >= 1 && j >= 1 && cells[i - 1][j - 1].isEmpty())
						cells[i - 1][j - 1].checkCell();
					if (i <= limit && j <= limit && cells[i + 1][j + 1].isEmpty())
						cells[i + 1][j + 1].checkCell();
					if (i >= 1 && j <= limit && cells[i - 1][j + 1].isEmpty())
						cells[i - 1][j + 1].checkCell();
					if (i <= limit && j >= 1 && cells[i + 1][j - 1].isEmpty())
						cells[i + 1][j - 1].checkCell();
				}
			}
		}
	}
	
	/*  tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public int getID() {
		int id = cellID;
		cellID++;
		return id;
	}
	/* tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */
	public Cell getCell(int id) {
		for (Cell[] a : cells) {
			for (Cell b : a) {
				if (b.getId() == id)
					return b;

			}
		}
		return null;
	}

	/* this method reveals the field if game is over
	 * tutorial from :http://codereview.stackexchange.com/questions/88636/beginner-minesweeper-game */ 
	public void fail() {
		for (Cell[] a : cells) {
			for (Cell b : a) {
				b.reveal();
				
			}

		}
	}
}