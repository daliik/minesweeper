package minesweeper;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GameOver {
	private static Frame Gframe;

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameOver window = new GameOver();

					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GameOver() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 150, 150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton restart = new JButton("Taaskäivita");
		restart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		restart.setBounds(12, 49, 117, 25);
		frame.getContentPane().add(restart);

		JButton Close = new JButton("Sulge");
		Close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});
		Close.setBounds(12, 87, 117, 25);

		frame.getContentPane().add(Close);

		{
		}
	}
}
