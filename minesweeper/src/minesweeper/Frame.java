package minesweeper;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Frame extends JFrame {

	private JPanel contentPane;
	public static MinesweeperInterface msi = new MinesweeperInterface();
	private static Board board;

	/*
	 * 
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame frame = new Frame();
					board = new Board();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	int a;

	/*
	 * Creates the frame for the game
	 */
	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// adds 3 new buttons and if the button is pressed, sends value to Board
		// class.

		JButton big = new JButton("50 x 50");
		big.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == big) {
					a = 50;
					Frame.msi.setFrameSize(a);
					board.getBoard();
				}
			}
		});
		big.setBounds(155, 162, 117, 25);
		contentPane.add(big);

		JButton average = new JButton("25 x 25");
		average.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == average) {
					a = 25;
					Frame.msi.setFrameSize(a);
					board.getBoard();
				}
			}
		});

		average.setBounds(155, 123, 117, 25);
		contentPane.add(average);

		JButton small = new JButton("10 x 10");
		small.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == small) {
					a = 10;
					Frame.msi.setFrameSize(a);
					board.getBoard();

				}
			}
		});

		small.setBounds(155, 86, 117, 25);
		contentPane.add(small);

		// Makes two labels on the welcome screen.

		JLabel lblValiRaskusaste = new JLabel("Vali mänguvälja suurus");
		lblValiRaskusaste.setBounds(131, 61, 194, 25);
		contentPane.add(lblValiRaskusaste);

		JLabel lblTereTulemast = new JLabel("Tere tulemast!");
		lblTereTulemast.setBounds(155, 34, 117, 15);
		contentPane.add(lblTereTulemast);
	}

}